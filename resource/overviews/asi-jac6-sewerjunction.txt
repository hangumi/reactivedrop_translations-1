﻿GAME
{
	"pos_x"	"-6090"
	"pos_y"	"3762"
	"scale"	"11"
	"material" "vgui/swarm/Overviews/SewersOverview"
	"briefingmaterial" "vgui/swarm/Overviews/SewersBriefingOverview"
	"missiontitle"		"하수 분기점 B5 "
	//"description"	"Explore the underground sewer passages beneath the colony."
	"description"	"식민지 아래에 있는 지하 하수도를 탐사하십시오."
	"image"		"swarm/ObjectivePics/obsewertunnel"
	"version"		"1"
	"author"		"Valve"
	"website"		"http://www.alienswarm.com/"
	"builtin"		"1"
}
