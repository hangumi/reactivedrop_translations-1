﻿GAME
{
	// radar map position and scale (see the Valve wiki for more information: http://developer.valvesoftware.com/wiki/Main_Page)
	"pos_x"		"-5860"
	"pos_y"		"4180"
	"scale"		"10.0"

	// material used for the radar map (in the bottom left corner)
	"material" 		"vgui/swarm/overviews/rd_ocs1overview"  		  		// example: "vgui/swarm/Overviews/landingbay02Overview"

	// material used for the briefing map (shown before the mission starts)
	"briefingmaterial" 	"vgui/swarm/overviews/rd_ocs1overview"		// example: "vgui/swarm/Overviews/landingbay02briefingOverview"

	// Title of the mission
	"missiontitle"		"보관 시설 "
	// Description text shown for the mission
	//"description"		"Gain entrance to the storage facility and find the way to landing bay 7."
	"description"		"보관 시설로 들어가 7 착륙장으로 가는 길을 찾으십시오."

	// Mission image that shows when selecting this mission
	"image"			"swarm/missionpics/rd_ocs1mapicon"  			//must be under "vgui" - dont include vgui in the path

	"builtin" 		"1"
	"version"		"1.3"
	"author"		"Steve76"
	"website"		"http://www.moddb.com/games/alien-swarm/addons/operation-cleansweep"
}
