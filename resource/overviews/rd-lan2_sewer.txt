﻿GAME
{
"pos_x" "-8502"
"pos_y" "4826"
"scale" "10.0"
"material" "vgui/swarm/overviews/rd-lan2_seweroverview"
"briefingmaterial" "vgui/swarm/overviews/rd-lan2_sewerbriefingoverview"
"mapyoffset" "-60"
"missiontitle"		"라나의 하수도 "
//"description"	"Proceed to maintenance facility"
"description"	"유지관리 시설로 나아가십시오"
"image"		"swarm/Missionpics/rd-lan2_sewermpic"

"builtin" 		"1"
"version"		"1.06"
"author"		"Hephalistofene"
"website"		""
}
