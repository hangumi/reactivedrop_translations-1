﻿GAME
{
"pos_x" 	"-7078"
"pos_y" 	"4838"
"scale" 	"10"

"material" 			"vgui/swarm/overviews/rd-PP_groundOverview"

"briefingmaterial" 	"vgui/swarm/overviews/rd-PP_briefgroundOverview"

"mapyoffset" 		"-60"

"missiontitle"		"발전소 냉각 펌프 "
//"description"		"Bring Power Online"
"description"		"전원을 다시 켜십시오"

"image"				"swarm/MissionPics/rd-area9800PP1pic"

"builtin" 			"1"
"version" 			"1.5"
"author" 			"DK9800 productions"
"website" 			"http://www.xs4all.nl/~arjend/"
}
