﻿GAME
{
	"pos_x"		"-9218"
	"pos_y"		"8073"
	"scale"		"12.0"
	"material" 		"vgui/swarm/Overviews/rd_par_Mission2Overview"
	"briefingmaterial" 	"vgui/swarm/Overviews/rd_par_Mission2BriefingOverview"
	"missiontitle"		"추가 임무 5 "
	//"description"		"After cleaning the first level, you decide to learn about recent activities of the base."
	"description"		"첫 번째 층을 싹쓸이 한 후, 당신은 기지의 최근 활동들을 알아내기로 결정했습니다."
	"image"			"swarm/MissionPics/rd_par_mission2"

	"builtin" 		"1"
	"version"		"1"
	"author"		"Eclipse"
	"website"		""
}
