﻿GAME
{
	"pos_x"             "6335"
	"pos_y"             "5412"
	"scale"             "5.0"
	"material"          	"vgui/swarm/overviews/rd_bonusmis3_overview"
	"briefingmaterial"  	"vgui/swarm/overviews/rd_bonusmis3_overview"
	//"mapyoffset" "-60"
	"missiontitle"		  "추가 임무 3 "
	"description"	      ""
	"image"		          "swarm/objectivepics/rd_bon3_blueprint"
	
	"builtin" 			  "1"
	"version"		      "1.0"
	"author"			  "uradzimiru"
	"website"		      ""
}

// Overview: scale 5.00, pos_x 6975, pos_y 5412
