﻿GAME
{
	// radar map position and scale (see the Valve wiki for more information: http://developer.valvesoftware.com/wiki/Main_Page)
	"pos_x"	"-5993"
	"pos_y"	"-1627"
	"scale"	"9.3"

	"material" "vgui/swarm/Overviews/residentialOverview"
	"briefingmaterial" "vgui/swarm/Overviews/residentialBriefingOverview"

	// Title of the mission
	"missiontitle"		"거주지 데스매치 "
	// Description text shown for the mission
	//"description"		"Second deathmatch map."
	"description"		"두번째 데스매치 맵."
	
	// Mission image that shows when selecting this mission
	"image"			"swarm/ObjectivePics/obresidentialbio"  			//must be under "vgui" - dont include vgui in the path

	"builtin" 		"1"
	"version"		"0.1"
	"author"		"Valve"
	"website"		"http://www.moddb.com/mods/reactivedrop"
}
