﻿GAME
{
	// radar map position and scale (see the Valve wiki for more information: http://developer.valvesoftware.com/wiki/Main_Page)
	"pos_x"		"-6420"
	"pos_y"		"4350"
	"scale"		"10.0"

	// material used for the radar map (in the bottom left corner)
	"material" 		"vgui/swarm/overviews/rd_ocs2overview"  			// example: "vgui/swarm/Overviews/landingbay02Overview"

	// material used for the briefing map (shown before the mission starts)
	"briefingmaterial" 	"vgui/swarm/overviews/rd_ocs2overview"

	// Title of the mission
	//"missiontitle"		"Landing Bay 7"
	"missiontitle"		"7번 착륙장 "
	// Description text shown for the mission
	//"description"		"Locate the quarantined shuttle somewhere in the landing bay and use it to infiltrate the infested starcruiser."
	"description"		"착륙장 안 어딘가에 있는 격리된 왕복선을 찾고 그것을 이용하여 감염된 항성순양함에 잠입하십시오."
	
	// Mission image that shows when selecting this mission
	"image"			"swarm/missionpics/rd_ocs2mapicon"  			//must be under "vgui" - dont include vgui in the path

	"builtin" 		"1"
	"version"		"1.3"
	"author"		"Steve76"
	"website"		"http://www.moddb.com/games/alien-swarm/addons/operation-cleansweep"
}
