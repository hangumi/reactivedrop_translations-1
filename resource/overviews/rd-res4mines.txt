﻿GAME
{
	"pos_x"		"-14979"
	"pos_y"		"2962"
	"scale"		"10.0"

	"material" 		"vgui/swarm/overviews/rd_res_themines2overview"
	"briefingmaterial" 	"vgui/swarm/overviews/rd_res_themines2overviewbriefing"
	
	//"missiontitle"		"Jericho Mines"
	"missiontitle"		"제리코 광산 "
	//"description"		"Investigate and eradicate the infection within the Jericho Mines."
	"description"		"제리코 광산 내의 감염을 조사하고 박멸하십시오."

	"image"			"swarm/MissionPics/rd_res_M4MissionPic"

	"builtin" 		"1"
	"version"		"1.3"
	"author"		"Will Rogers"
	"website"		"https://sites.google.com/site/swarmaddons/maps/research7"
}
